# WireEvening

This is a style I've put together to give wire a break on the eyes. It is based on [IRCCloud.com's](https://www.irccloud.com/) Ash theme. They did a really good job with the colors over there.

## Install

[![Install directly with Stylus](https://img.shields.io/badge/Install%20directly%20with-Stylus-3daee9.svg?longCache=true&style=for-the-badge)](https://gitlab.com/kinghat/wireevening/raw/master/wireevening.user.css)

## Screenshots

![alt text](./images/main.png "main page")
![alt text](./images/preferences.png "preferences page")
